use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use std::sync::Mutex;

use once_cell::sync::Lazy;

use crate::rtspinput::*;

extern crate chrono;
extern crate timer;

use chrono::Duration;

#[derive(Default)]
struct Settings {
    uri: Option<String>,
}

struct State {
    shutdown: bool,
    update_offset: bool,
    srcbin: Option<RtspInput>,
    appsrc: gst_app::AppSrc,
    timer: timer::Timer,
    guard: Option<timer::Guard>,
}

impl Default for State {
    fn default() -> Self {
        let appsrc = gst::ElementFactory::make("appsrc", None)
            .unwrap()
            .downcast::<gst_app::AppSrc>()
            .unwrap();

        appsrc.set_property("handle-segment-change", true);
        appsrc.set_property_from_str("stream-type", "stream");
        appsrc.set_property_from_str("format", "time");
        appsrc.set_property("is-live", true);

        Self {
            shutdown: false,
            update_offset: false,
            srcbin: None,
            timer: timer::Timer::new(),
            guard: None,
            appsrc,
        }
    }
}

pub struct RtspSrcBin {
    settings: Mutex<Settings>,
    state: Mutex<State>,
}

#[glib::object_subclass]
impl ObjectSubclass for RtspSrcBin {
    const NAME: &'static str = "RtspSrcBin";
    type Type = super::RtspSrcBin;
    type ParentType = gst::Bin;

    fn new() -> Self {
        Self {
            settings: Mutex::new(Settings::default()),
            state: Mutex::new(State::default()),
        }
    }
}

impl ObjectImpl for RtspSrcBin {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![glib::ParamSpecString::new(
                "uri",
                "URI",
                "URI to use",
                None,
                glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
            )]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "uri" => {
                let uri: Option<String> = value.get().expect("type checked upstream");
                let mut settings = self.settings.lock().unwrap();
                settings.uri = uri;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "uri" => {
                let settings = self.settings.lock().unwrap();
                settings.uri.to_value()
            }
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.set_suppressed_flags(gst::ElementFlags::SOURCE | gst::ElementFlags::SINK);
        obj.set_element_flags(gst::ElementFlags::SOURCE);

        let identity = gst::ElementFactory::make("identity", None).unwrap();
        // We don't want to update segment per retry
        identity.set_property("single-segment", true);

        let state = self.state.lock().unwrap();
        obj.add_many(&[state.appsrc.upcast_ref(), &identity])
            .unwrap();

        let srcpad =
            gst::GhostPad::with_target(Some("src"), &identity.static_pad("src").unwrap()).unwrap();
        obj.add_pad(&srcpad).unwrap();

        state.appsrc.link(&identity).unwrap();

        let pad = state.appsrc.static_pad("src").unwrap();
        let obj_weak = obj.downgrade();
        pad.add_probe(
            gst::PadProbeType::EVENT_DOWNSTREAM | gst::PadProbeType::BUFFER,
            move |pad, info| {
                let obj = match obj_weak.upgrade() {
                    Some(obj) => obj,
                    None => return gst::PadProbeReturn::Remove,
                };

                let imp = obj.imp();
                match info.data {
                    Some(gst::PadProbeData::Event(ref ev)) => {
                        if let gst::EventView::Eos(..) = ev.view() {
                            let mut state = imp.state.lock().unwrap();
                            if state.shutdown {
                                // Forward EOS if we are shutting down
                                return gst::PadProbeReturn::Ok;
                            }

                            println!("Dropping EOS");

                            if let Some(srcbin) = state.srcbin.take() {
                                drop(state);
                                let _ = srcbin.set_state(gst::State::Null);
                            }

                            println!("Input cleared");

                            return gst::PadProbeReturn::Drop;
                        }
                    }
                    Some(gst::PadProbeData::Buffer(ref buffer)) => {
                        let mut state = imp.state.lock().unwrap();
                        if state.update_offset {
                            if let Some(peer) = pad.peer() {
                                let cur_running_time =
                                    obj.current_running_time().unwrap_or(gst::ClockTime::ZERO);
                                let segment = match pad.sticky_event::<gst::event::Segment<_>>(0) {
                                    Some(ev) => ev.segment().clone(),
                                    _ => unreachable!(),
                                };

                                let segment = segment.downcast::<gst::format::Time>().unwrap();
                                let running_time = segment.to_running_time(buffer.pts()).unwrap();

                                let offset = if cur_running_time >= running_time {
                                    (cur_running_time - running_time).nseconds() as i64
                                } else {
                                    -((running_time - cur_running_time).nseconds() as i64)
                                };

                                println!("Set offset {}", offset);

                                peer.set_offset(offset);
                            }

                            state.update_offset = false;
                        }
                    }
                    _ => (),
                }

                gst::PadProbeReturn::Ok
            },
        );
    }
}

impl GstObjectImpl for RtspSrcBin {}

impl ElementImpl for RtspSrcBin {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "RtspSrcBin",
                "Source",
                "RtspSrcBin element",
                "Seungha Yang <seungha@centricular.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    #[allow(clippy::single_match)]
    fn change_state(
        &self,
        elem: &Self::Type,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        match transition {
            gst::StateChange::PausedToPlaying => {
                let mut state = self.state.lock().unwrap();
                state.shutdown = false;

                // Run timer to retry
                let obj_weak = elem.downgrade();
                state.guard = Some(state.timer.schedule_repeating(
                    Duration::seconds(5),
                    move || {
                        let obj = obj_weak.upgrade().unwrap();

                        println!("Timer tick");

                        let imp = obj.imp();
                        let state = imp.state.lock().unwrap();
                        if state.srcbin.is_none() {
                            drop(state);
                            imp.start_input();
                        }
                    },
                ));

                drop(state);

                self.start_input();
            }
            _ => (),
        }

        self.parent_change_state(elem, transition).map_err(|err| {
            println!("parent state change error {:?}", err);

            err
        })?;

        match transition {
            gst::StateChange::PlayingToPaused => {
                let mut state = self.state.lock().unwrap();
                state.guard = None;
                state.shutdown = true;

                if let Some(srcbin) = state.srcbin.take() {
                    drop(state);
                    let _ = srcbin.set_state(gst::State::Null);
                }
                Ok(gst::StateChangeSuccess::NoPreroll)
            }
            gst::StateChange::ReadyToPaused => Ok(gst::StateChangeSuccess::NoPreroll),
            _ => Ok(gst::StateChangeSuccess::Success),
        }
    }

    fn send_event(&self, element: &Self::Type, event: gst::Event) -> bool {
        if event.type_() == gst::EventType::Eos {
            println!("Got EOS event");
            let mut state = self.state.lock().unwrap();
            state.shutdown = true;
        }

        self.parent_send_event(element, event)
    }
}

impl BinImpl for RtspSrcBin {}

impl RtspSrcBin {
    pub fn shutdown(&self) {
        let mut state = self.state.lock().unwrap();
        state.shutdown = true;
    }

    fn start_input(&self) {
        let state = self.state.lock().unwrap();
        let settings = self.settings.lock().unwrap();

        let srcbin = RtspInput::new(None, settings.uri.as_ref().unwrap());
        srcbin.set_appsrc(&state.appsrc);
        let appsrc = state.appsrc.clone();
        drop(settings);
        drop(state);

        // To clear EOS
        let _ = appsrc.set_state(gst::State::Null);
        appsrc.sync_state_with_parent().unwrap();

        let mut state = self.state.lock().unwrap();
        state.srcbin = Some(srcbin.clone());
        state.update_offset = true;
        drop(state);

        let bus = srcbin.bus().unwrap();
        let _ = bus.add_watch(move |_, msg| {
            use gst::MessageView;

            match msg.view() {
                MessageView::Error(_) => {
                    // Error message shouldn't happen
                    unreachable!()
                }
                MessageView::Warning(err) => {
                    println!("Got warning {:?}", err);
                }
                _ => (),
            }

            glib::Continue(true)
        });
        let _ = srcbin.set_state(gst::State::Playing);
    }
}
