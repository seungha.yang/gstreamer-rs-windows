use gst::glib;

mod imp;

glib::wrapper! {
    pub struct RtspSrcBin(ObjectSubclass<imp::RtspSrcBin>) @extends gst::Bin, gst::Element, gst::Object;
}

unsafe impl Send for RtspSrcBin {}
unsafe impl Sync for RtspSrcBin {}

impl RtspSrcBin {
    pub fn new(name: Option<&str>, uri: &str) -> Self {
        glib::Object::new(&[("name", &name), ("uri", &uri)]).unwrap()
    }
}
