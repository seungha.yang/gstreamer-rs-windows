use gst::glib;
use gst::prelude::*;

use anyhow::Error;
use clap::Parser;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Seek};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex, Weak};
use std::thread;
use win32console::{console::WinConsole, input::KeyEvent};

use helpers::rtspsrcbin::*;

#[derive(Parser)]
struct Cli {
    uri: String,
    #[clap(parse(from_os_str))]
    output: PathBuf,
}

#[derive(Debug, Clone)]
struct Fragment {
    start_time: gst::ClockTime,
    duration: gst::ClockTime,
    position: u64,
    size: usize,
    // Associated header fragment index for given data segment.
    // unused in case of header fragment
    header_index: usize,
}

impl Default for Fragment {
    fn default() -> Self {
        Self {
            start_time: gst::ClockTime::ZERO,
            duration: gst::ClockTime::ZERO,
            position: 0,
            size: 0,
            header_index: 0,
        }
    }
}

impl Fragment {
    fn compare(&self, other: &Self) -> std::cmp::Ordering {
        self.start_time.cmp(&other.start_time)
    }
}

#[derive(Debug, Clone)]
struct Manifest {
    headers: Vec<Fragment>,
    fragments: Vec<Fragment>,
    // Start timestamp of the first fragment
    presentation_offset: gst::ClockTime,
    total_duration: gst::ClockTime,
}

impl Default for Manifest {
    fn default() -> Self {
        Self {
            headers: Vec::new(),
            fragments: Vec::new(),
            presentation_offset: gst::ClockTime::ZERO,
            total_duration: gst::ClockTime::ZERO,
        }
    }
}

#[derive(PartialEq, Copy, Clone)]
enum ManifestState {
    Running,
    Seeking,
    Eos,
    Error,
}

struct ManifestReaderInner {
    header_reader: BufReader<File>,
    reader: BufReader<File>,
    manifest: Manifest,
    header_index: usize,
    fragment_index: usize,
    push_header: bool,
    state: ManifestState,
    discont: bool,
    segment: gst::FormattedSegment<gst::format::Time>,
    caps: gst::Caps,
}

#[derive(Clone)]
struct ManifestReader(Arc<Mutex<ManifestReaderInner>>);

struct ManifestReaderWeak(Weak<Mutex<ManifestReaderInner>>);

impl ManifestReaderWeak {
    pub fn upgrade(&self) -> Option<ManifestReader> {
        self.0.upgrade().map(ManifestReader)
    }
}

impl ManifestReader {
    fn new(path: &PathBuf, manifest: &Manifest) -> Result<Self, Error> {
        // Open two file struct for the same path, to make header reading easier
        let header_file = File::open(path)?;
        let file = File::open(path)?;
        let mut segment = gst::FormattedSegment::<gst::format::Time>::new();
        segment.set_start(manifest.presentation_offset);
        segment.set_position(manifest.presentation_offset);
        segment.set_stop(manifest.presentation_offset + manifest.total_duration);
        let inner = Arc::new(Mutex::new(ManifestReaderInner {
            header_reader: BufReader::new(header_file),
            reader: BufReader::new(file),
            manifest: manifest.clone(),
            header_index: 0,
            fragment_index: 0,
            push_header: true,
            state: ManifestState::Running,
            discont: true,
            caps: gst::Caps::builder("video/quicktime").build(),
            segment,
        }));

        Ok(ManifestReader(inner))
    }

    fn downgrade(&self) -> ManifestReaderWeak {
        ManifestReaderWeak(Arc::downgrade(&self.0))
    }

    fn duration(&self) -> gst::ClockTime {
        let inner = self.0.lock().unwrap();
        inner.manifest.total_duration
    }

    // Performs segment seek, and update rate.
    // Additional segment adjustment is done based on presentation
    // start time
    fn prepare_seek(&self, event: &gst::event::Seek<&gst::EventRef>) {
        let mut inner = self.0.lock().unwrap();
        inner.state = ManifestState::Seeking;

        let (rate, flags, start_type, start, stop_type, stop) = event.get();

        let start: Option<gst::ClockTime> = match start.try_into() {
            Ok(start) => start,
            Err(_) => {
                inner.state = ManifestState::Error;
                return;
            }
        };

        let stop: Option<gst::ClockTime> = match stop.try_into() {
            Ok(stop) => stop,
            Err(_) => {
                inner.state = ManifestState::Error;
                return;
            }
        };

        inner
            .segment
            .do_seek(rate, flags, start_type, start, stop_type, stop);

        let offset = inner.manifest.presentation_offset;
        // Start time must be valid already
        let start = inner.segment.start().unwrap() + offset;
        inner.segment.set_start(start);
        if inner.segment.rate() >= 0.0 && start_type != gst::SeekType::None {
            inner.segment.set_position(start);
        }

        if let Some(stop) = inner.segment.stop() {
            let stop = stop + offset;
            inner.segment.set_stop(stop);
            if inner.segment.rate() < 0.0 && stop_type != gst::SeekType::None {
                inner.segment.set_position(stop);
            }
        };

        println!("Configured segment {:?}", inner.segment);
    }

    fn state(&self) -> ManifestState {
        let inner = self.0.lock().unwrap();
        inner.state
    }

    fn seek(&self, pos: gst::ClockTime) -> gst::FlowReturn {
        let mut inner = self.0.lock().unwrap();
        inner.state = ManifestState::Running;
        if pos >= inner.manifest.total_duration {
            println!(
                "Requested position {} > total duration {}",
                pos, inner.manifest.total_duration
            );
            inner.state = ManifestState::Eos;
            return gst::FlowReturn::Eos;
        }

        let target = Fragment {
            start_time: pos,
            ..Fragment::default()
        };

        let rst = inner
            .manifest
            .fragments
            .binary_search_by(|f| f.compare(&target));

        inner.fragment_index = match rst {
            Ok(index) => {
                // fragment time is identical to fragment start time
                // positive rate: Use the fragment
                // negative rate: Use previous fragment
                if inner.segment.rate() >= 0f64 {
                    index
                } else {
                    if index == 0 {
                        println!("negative rate and with target index 0");
                        return gst::FlowReturn::Eos;
                    }

                    index - 1
                }
            }
            Err(index) => {
                // searched index indicates an index that can be inserted
                // Always use the previous index
                if index != 0 {
                    index - 1
                } else {
                    0
                }
            }
        };

        println!(
            "Target fragment index {} for time seek {}, start time {}, duration {}",
            inner.fragment_index,
            pos,
            inner.manifest.fragments[inner.fragment_index].start_time,
            inner.manifest.fragments[inner.fragment_index].duration
        );

        let byte_pos = inner.manifest.fragments[inner.fragment_index].position;
        match inner.reader.seek(std::io::SeekFrom::Start(byte_pos)) {
            Ok(_) => (),
            Err(_) => {
                inner.state = ManifestState::Error;
                return gst::FlowReturn::Error;
            }
        }
        inner.header_index = inner.manifest.fragments[inner.fragment_index].header_index;
        inner.push_header = true;
        inner.discont = true;

        gst::FlowReturn::Ok
    }

    fn advance_fragment(&self, inner: &mut ManifestReaderInner) {
        if inner.segment.rate() >= 0f64 {
            if inner.fragment_index + 1 >= inner.manifest.fragments.len() {
                inner.state = ManifestState::Eos;
                return;
            }

            inner.fragment_index += 1;
        } else {
            if inner.fragment_index == 0 {
                inner.state = ManifestState::Eos;
                return;
            }

            inner.fragment_index -= 1;
            // Needs to mark discont on buffer in case of reverse playback,
            // so that demuxer reset previous fragment information
            inner.discont = true;
        }

        println!("Advancing fragment to {}", inner.fragment_index);

        let byte_pos = inner.manifest.fragments[inner.fragment_index].position;
        match inner.reader.seek(std::io::SeekFrom::Start(byte_pos)) {
            Ok(_) => (),
            Err(_) => {
                inner.state = ManifestState::Error;
                return;
            }
        }

        let header_index = inner.manifest.fragments[inner.fragment_index].header_index;
        if inner.header_index != header_index {
            inner.header_index = header_index;
            inner.push_header = true;
            inner.discont = true;
        }
    }

    fn current_fragment(&self, inner: &ManifestReaderInner) -> Option<Fragment> {
        if inner.state != ManifestState::Running {
            return None;
        }

        Some(inner.manifest.fragments[inner.fragment_index].clone())
    }

    fn read_data(&self) -> Option<gst::Sample> {
        let mut inner = self.0.lock().unwrap();
        if inner.push_header {
            let header = &inner.manifest.headers[inner.header_index];
            let pos = header.position;
            let size = header.size;
            match inner.header_reader.seek(std::io::SeekFrom::Start(pos)) {
                Ok(_) => (),
                Err(err) => {
                    println!("file seek to {} failed", err);
                    return None;
                }
            }

            let mut buffer = gst::Buffer::with_size(size as usize).unwrap();
            {
                let buf_mut = buffer.make_mut();
                let mut map = buf_mut.map_writable().unwrap();

                // Header buffer should be small enough, read the data at once
                inner.header_reader.read_exact(map.as_mut()).unwrap();
                drop(map);

                let pts = if inner.segment.rate() < 0.0 {
                    inner.segment.stop().unwrap()
                } else {
                    inner.segment.start().unwrap()
                };

                buf_mut.set_offset(pos);
                buf_mut.set_offset_end(pos + size as u64);
                buf_mut.set_flags(gst::BufferFlags::DISCONT | gst::BufferFlags::HEADER);
                buf_mut.set_pts(pts);
            }

            inner.push_header = false;
            let sample = gst::Sample::builder()
                .buffer(&buffer)
                .caps(&inner.caps)
                .segment(&inner.segment)
                .build();

            return Some(sample);
        }

        let fragment = self.current_fragment(&inner);
        let fragment = fragment?;

        let mut buffer = gst::Buffer::with_size(fragment.size).unwrap();
        {
            let buf_mut = buffer.make_mut();
            let mut map = buf_mut.map_writable().unwrap();

            // XXX: In case of reverse playback, basesrc will set DISCONT buffer
            // flag to each buffer, which will confuse qtdemux.
            // (qtdemux refers to the DISCONT flag in case of fmp4 reverse playback)
            // Should push the fragment data at once to workaround it
            inner.reader.read_exact(map.as_mut()).unwrap();
            drop(map);

            buf_mut.set_offset(fragment.position);
            buf_mut.set_offset_end(fragment.position + fragment.size as u64);

            println!("Returning buffer with pts {}", fragment.start_time);
            buf_mut.set_pts(fragment.start_time);

            if inner.discont {
                buf_mut.set_flags(gst::BufferFlags::DISCONT);
                inner.discont = false;
            }
        }

        self.advance_fragment(&mut inner);

        let sample = gst::Sample::builder()
            .buffer(&buffer)
            .caps(&inner.caps)
            .segment(&inner.segment)
            .build();
        Some(sample)
    }
}

// Record ES stream using fmp4 muxer, and create manifest
// proxysrc ! parse ! timestamper ! queue ! muxer ! appsink
struct RecordBinInner {
    pipeline: gst::Pipeline,
    manifest: Arc<Mutex<Manifest>>,
    join_handle: Option<thread::JoinHandle<()>>,
}

struct RecordBin {
    inner: Mutex<RecordBinInner>,
}

impl RecordBin {
    fn new(proxy_sink: &gst::Element, codec: &str, output: &PathBuf) -> Self {
        let mut file = File::create(output).unwrap();

        let pipeline = gst::Pipeline::new(None);
        let src = gst::ElementFactory::make("proxysrc", None).unwrap();
        src.set_property("proxysink", proxy_sink);
        let (parser, timestamper) = match codec {
            "video/x-h264" => {
                let parser = gst::ElementFactory::make("h264parse", None).unwrap();
                let timestamper = gst::ElementFactory::make("h264timestamper", None).unwrap();
                (parser, timestamper)
            }
            "video/x-h265" => {
                let parser = gst::ElementFactory::make("h265parse", None).unwrap();
                let timestamper = gst::ElementFactory::make("h265timestamper", None).unwrap();
                (parser, timestamper)
            }
            _ => unreachable!(),
        };
        let queue = gst::ElementFactory::make("queue", None).unwrap();
        let muxer = gst::ElementFactory::make("isofmp4mux", None).unwrap();
        muxer.set_property("fragment-duration", gst::ClockTime::from_seconds(10));
        // Write fragment random access box at the end of fragment
        muxer.set_property("write-mfra", true);
        // Update moov header with mehd box (indicats total duration)
        // at the end of recording
        muxer.set_property_from_str("header-update-mode", "update");
        muxer.set_property("write-mehd", true);
        // Disable split on timeout if upstream is live source
        // otherwise some buffers might be dropped if stream is fragmented
        // at non-keyframe boundary because of timeout
        muxer.set_property(
            "min-upstream-latency",
            // One day should be sufficiently large enough
            gst::ClockTime::from_seconds(24 * 60 * 60),
        );
        let sink = gst::ElementFactory::make("appsink", None).unwrap();

        pipeline
            .add_many(&[&src, &parser, &timestamper, &queue, &muxer, &sink])
            .unwrap();
        gst::Element::link_many(&[&src, &parser, &timestamper, &queue, &muxer, &sink]).unwrap();

        let manifest = Arc::new(Mutex::new(Manifest::default()));
        let manifest_clone = manifest.clone();
        let manifest_other_clone = manifest.clone();
        let appsink = sink.downcast::<gst_app::AppSink>().unwrap();
        appsink.set_buffer_list(true);
        appsink.set_property("sync", false);
        appsink.set_property("async", false);
        appsink.set_callbacks(
            gst_app::AppSinkCallbacks::builder()
                .new_sample(move |sink| {
                    let sample = sink.pull_sample().map_err(|_| gst::FlowError::Eos)?;

                    let mut manifest = manifest_clone.lock().unwrap();
                    let mut buffer_list = sample.buffer_list_owned().unwrap();
                    let segment = sample
                        .segment()
                        .unwrap()
                        .downcast_ref::<gst::ClockTime>()
                        .unwrap();
                    let mut first = buffer_list.get(0).unwrap();

                    // This is moov header, store this fragment separately
                    if first
                        .flags()
                        .contains(gst::BufferFlags::DISCONT | gst::BufferFlags::HEADER)
                    {
                        if manifest.headers.is_empty() {
                            let position = file.seek(std::io::SeekFrom::Current(0)).unwrap();
                            let map = first.map_readable().unwrap();
                            file.write_all(&map).expect("failed to write header");
                            drop(map);

                            let end_position = file.seek(std::io::SeekFrom::Current(0)).unwrap();

                            let header = Fragment {
                                start_time: gst::ClockTime::ZERO,
                                duration: gst::ClockTime::ZERO,
                                position,
                                size: (end_position - position) as usize,
                                header_index: manifest.fragments.len(),
                            };

                            println!("Written header {:?}", header);
                            manifest.headers.push(header);

                            buffer_list.make_mut().remove(0, 1);
                            if buffer_list.is_empty() {
                                return Ok(gst::FlowSuccess::Ok);
                            }

                            first = buffer_list.get(0).unwrap();
                        } else {
                            let prev_header = &manifest.headers[0];
                            println!("Rewriting header");

                            let map = first.map_readable().unwrap();

                            // moov header size was reserved at the beginning of
                            // recording. New header size must be equal to the
                            // previous one
                            assert!(map.size() == prev_header.size);
                            file.seek(std::io::SeekFrom::Start(prev_header.position))
                                .unwrap();
                            file.write_all(&map).expect("failed to write header");
                            drop(map);

                            return Ok(gst::FlowSuccess::Ok);
                        }
                    }

                    assert!(!manifest.headers.is_empty());

                    let position = file.seek(std::io::SeekFrom::Current(0)).unwrap();
                    // Actual buffer timestamp might not start from zero.
                    // Holds running time instead of buffer pts
                    let (start_time, duration) = if let Some(pts) = first.pts() {
                        let start_time = segment.to_running_time(pts).unwrap();
                        let duration = first.duration().unwrap();
                        (start_time, duration)
                    } else {
                        (gst::ClockTime::ZERO, gst::ClockTime::ZERO)
                    };

                    for buffer in &*buffer_list {
                        let map = buffer.map_readable().unwrap();
                        file.write_all(&map).expect("failed to write fragment");
                    }

                    let end_position = file.seek(std::io::SeekFrom::Current(0)).unwrap();
                    let fragment = Fragment {
                        start_time,
                        duration,
                        position,
                        size: (end_position - position) as usize,
                        header_index: manifest.headers.len() - 1,
                    };

                    // Actual start time of this stream
                    if manifest.fragments.is_empty() {
                        manifest.presentation_offset = start_time;
                    }

                    // This is footer atom (i.e., mfra box)
                    if start_time == gst::ClockTime::ZERO && duration == gst::ClockTime::ZERO {
                        println!(
                            "Written footer fragment {:?}, current header len {}",
                            fragment,
                            manifest.headers.len()
                        );

                        manifest.headers.push(fragment);
                    } else {
                        println!(
                            "Written fragment {:?}, fragment index {}",
                            fragment,
                            manifest.fragments.len()
                        );

                        manifest.fragments.push(fragment);
                    }

                    Ok(gst::FlowSuccess::Ok)
                })
                .eos(move |_sink| {
                    // Calculates total duration on EOS
                    let mut manifest = manifest_other_clone.lock().unwrap();
                    if !manifest.fragments.is_empty() {
                        let first = &manifest.fragments[0];
                        let last = &manifest.fragments[manifest.fragments.len() - 1];
                        let start_time = first.start_time;
                        let end_time = last.start_time + last.duration;

                        manifest.total_duration = end_time - start_time;
                    }
                })
                .build(),
        );

        Self {
            inner: Mutex::new(RecordBinInner {
                pipeline,
                manifest,
                join_handle: None,
            }),
        }
    }

    // Spawn pipeline thread
    fn run(&self) {
        let mut inner = self.inner.lock().unwrap();
        if inner.join_handle.is_some() {
            return;
        }

        let pipeline_clone = inner.pipeline.clone();
        inner.join_handle = Some(thread::spawn(move || {
            println!("Begin recording pipeline");

            pipeline_clone.set_state(gst::State::Playing).unwrap();

            let bus = pipeline_clone.bus().unwrap();
            for msg in bus.iter_timed(gst::ClockTime::NONE) {
                use gst::MessageView;

                match msg.view() {
                    MessageView::Eos(..) => {
                        println!("received eos from recordbin");
                        break;
                    }
                    MessageView::Error(err) => {
                        println!(
                            "Error from {:?}: {} ({:?})",
                            err.src().map(|s| s.path_string()),
                            err.error(),
                            err.debug()
                        );
                        break;
                    }
                    _ => (),
                }
            }

            pipeline_clone.set_state(gst::State::Null).unwrap();

            println!("End recording pipeline");
        }));
    }

    fn stop(&self) {
        let mut inner = self.inner.lock().unwrap();

        inner.pipeline.send_event(gst::event::Eos::new());
        if let Some(join_handle) = inner.join_handle.take() {
            let _ = join_handle.join();
        }
    }

    fn manifest(&self) -> Manifest {
        let inner = self.inner.lock().unwrap();
        let manifest_lock = inner.manifest.lock().unwrap();
        manifest_lock.clone()
    }
}

struct DisplaybinInner {
    pipeline: gst::Pipeline,
    join_handle: Option<thread::JoinHandle<()>>,
}

struct Displaybin {
    inner: Mutex<DisplaybinInner>,
}

impl Displaybin {
    fn new(proxy_sink: &gst::Element) -> Self {
        let pipeline = gst::Pipeline::new(None);
        let src = gst::ElementFactory::make("proxysrc", None).unwrap();
        src.set_property("proxysink", proxy_sink);

        let queue = gst::ElementFactory::make("queue", None).unwrap();
        // Holds small buffers
        queue.set_property("max-size-buffers", 3u32);
        queue.set_property("max-size-time", gst::ClockTime::ZERO);
        queue.set_property("max-size-bytes", 0u32);

        let sink = gst::ElementFactory::make("d3d11videosink", None).unwrap();
        sink.set_property("sync", false);
        sink.set_property("async", false);

        pipeline.add_many(&[&src, &queue, &sink]).unwrap();
        gst::Element::link_many(&[&src, &queue, &sink]).unwrap();

        Self {
            inner: Mutex::new(DisplaybinInner {
                pipeline,
                join_handle: None,
            }),
        }
    }

    fn run(&self) {
        let mut inner = self.inner.lock().unwrap();
        if inner.join_handle.is_some() {
            return;
        }

        let pipeline_clone = inner.pipeline.clone();
        inner.join_handle = Some(thread::spawn(move || {
            println!("Begin display pipeline");

            pipeline_clone.set_state(gst::State::Playing).unwrap();

            let bus = pipeline_clone.bus().unwrap();
            for msg in bus.iter_timed(gst::ClockTime::NONE) {
                use gst::MessageView;

                match msg.view() {
                    MessageView::Eos(..) => {
                        println!("received eos from recordbin");
                        break;
                    }
                    MessageView::Error(err) => {
                        println!(
                            "Error from {:?}: {} ({:?})",
                            err.src().map(|s| s.path_string()),
                            err.error(),
                            err.debug()
                        );
                        break;
                    }
                    _ => (),
                }
            }

            pipeline_clone.set_state(gst::State::Null).unwrap();

            println!("End display pipeline");
        }));
    }

    fn stop(&self) {
        let mut inner = self.inner.lock().unwrap();

        inner.pipeline.send_event(gst::event::Eos::new());
        if let Some(join_handle) = inner.join_handle.take() {
            let _ = join_handle.join();
        }
    }
}

// uridecodebin ! tee name=t. ! fakesink
//                         t. ! proxysink (to Recordbin)
//                         t. ! decoder ! queue ! clocksync ! tee ! Displaybin
struct SourceBinInner {
    pipeline: gst::Pipeline,
    tee: gst::Element,
    decode_tee: gst::Element,
    decodebin: Option<gst::Element>,
    recordbin: Arc<Mutex<Option<RecordBin>>>,
    displaybin: Vec<Displaybin>,
}

#[derive(Clone)]
struct SourceBin {
    inner: Arc<Mutex<SourceBinInner>>,
}

impl SourceBin {
    fn new(uri: &str, output: &Path) -> Self {
        let pipeline = gst::Pipeline::new(None);
        let rtspbin = RtspSrcBin::new(None, uri);

        let recordbin = Arc::new(Mutex::new(None));

        let tee = gst::ElementFactory::make("tee", None).unwrap();
        let decode_tee = gst::ElementFactory::make("tee", None).unwrap();

        // Allow temporary non-linked
        tee.set_property("allow-not-linked", true);
        decode_tee.set_property("allow-not-linked", true);

        let fakesink = gst::ElementFactory::make("fakesink", None).unwrap();

        pipeline
            .add_many(&[rtspbin.upcast_ref(), &tee, &decode_tee, &fakesink])
            .unwrap();

        gst::Element::link_many(&[rtspbin.upcast_ref(), &tee, &fakesink]).unwrap();

        let tee_pad = tee.static_pad("sink").unwrap();
        let recordbin_clone = recordbin.clone();
        let pipeline_weak = pipeline.downgrade();
        let output_clone = output.to_path_buf();
        // Probe for recodebin configuration
        tee_pad.add_probe(gst::PadProbeType::EVENT_DOWNSTREAM, move |pad, info| {
            if let Some(gst::PadProbeData::Event(ref ev)) = info.data {
                if let gst::EventView::Caps(ref c) = ev.view() {
                    let caps = c.caps();
                    let s = caps.structure(0).unwrap();

                    match s.name() {
                        "video/x-h264" => (),
                        "video/x-h265" => (),
                        _ => unreachable!(),
                    };

                    let pipeline = match pipeline_weak.upgrade() {
                        Some(pipeline) => pipeline,
                        None => return gst::PadProbeReturn::Remove,
                    };

                    let tee = pad.parent().unwrap().downcast::<gst::Element>().unwrap();

                    let mut recordbin = recordbin_clone.lock().unwrap();
                    let proxysink = gst::ElementFactory::make("proxysink", None).unwrap();
                    pipeline.add(&proxysink).unwrap();
                    proxysink.sync_state_with_parent().unwrap();
                    tee.link(&proxysink).unwrap();
                    let rbin = RecordBin::new(&proxysink, s.name(), &output_clone);

                    rbin.run();
                    *recordbin = Some(rbin);

                    return gst::PadProbeReturn::Remove;
                }
            }

            gst::PadProbeReturn::Ok
        });

        Self {
            inner: Arc::new(Mutex::new(SourceBinInner {
                pipeline,
                tee,
                decode_tee,
                decodebin: None,
                recordbin,
                displaybin: Vec::new(),
            })),
        }
    }

    // Blocks calling thread and returns on EOS or ERROR
    fn run(&self) -> Manifest {
        let inner = self.inner.lock().unwrap();
        let pipeline_clone = inner.pipeline.clone();
        let recordbin_clone = inner.recordbin.clone();
        drop(inner);

        pipeline_clone.set_state(gst::State::Playing).unwrap();

        let bus = pipeline_clone.bus().unwrap();
        for msg in bus.iter_timed(gst::ClockTime::NONE) {
            use gst::MessageView;

            match msg.view() {
                MessageView::Eos(..) => {
                    println!("received eos from sourcebin");
                    break;
                }
                MessageView::Error(err) => {
                    println!(
                        "Error from {:?}: {} ({:?})",
                        err.src().map(|s| s.path_string()),
                        err.error(),
                        err.debug()
                    );
                    break;
                }
                MessageView::Latency(_) => {
                    println!("Got Latency message");
                    // Calculate latency again and propagate it to
                    // each element
                    let _ = pipeline_clone.recalculate_latency();
                }
                _ => (),
            }
        }

        pipeline_clone.set_state(gst::State::Null).unwrap();
        let mut inner = self.inner.lock().unwrap();
        inner.displaybin.retain(|d| {
            d.stop();
            false
        });
        drop(inner);

        let mut recordbin = recordbin_clone.lock().unwrap();
        if let Some(recordbin) = recordbin.take() {
            recordbin.stop();
            recordbin.manifest()
        } else {
            Manifest::default()
        }
    }

    fn stop(&self) {
        let inner = self.inner.lock().unwrap();
        inner.pipeline.send_event(gst::event::Eos::new());
    }

    // Creates decoder bin which consists of
    // [identity ! parse ! decoder ! convert ! queue ! clocksync ! identity]
    // - Front probe will drop buffers until keyframe
    // - Rear probe will drop upstream reconfigure event
    // - clocksync will calculate timestamp offset based on initial
    //   output frame from decoder, then the other decoded frames will
    //   by synchronized with clock by the clocksync element
    fn create_decodebin(&self) -> gst::Element {
        let dbin = gst::Bin::new(None);
        let front = gst::ElementFactory::make("identity", None).unwrap();
        let rear = gst::ElementFactory::make("identity", None).unwrap();

        // Add dummy front and rear element, then parser and decoder will
        // be configured later
        dbin.add_many(&[&front, &rear]).unwrap();

        let sinkpad = front.static_pad("sink").unwrap();
        dbin.add_pad(&gst::GhostPad::with_target(Some("sink"), &sinkpad).unwrap())
            .unwrap();

        let srcpad = rear.static_pad("src").unwrap();
        dbin.add_pad(&gst::GhostPad::with_target(Some("src"), &srcpad).unwrap())
            .unwrap();

        // Drops reconfigure event. It will trigger re-negotiation including
        // allocation query dancing. It's unnecessary for our use case
        srcpad.add_probe(gst::PadProbeType::EVENT_UPSTREAM, |_pad, info| {
            if let Some(gst::PadProbeData::Event(ref ev)) = info.data {
                if let gst::EventView::Reconfigure(_) = ev.view() {
                    return gst::PadProbeReturn::Drop;
                }
            }
            gst::PadProbeReturn::Ok
        });

        let dbin_weak = dbin.downgrade();
        let front_weak = front.downgrade();
        let rear_weak = rear.downgrade();
        // Probe caps event and buffers until keyframe is arrived,
        // then configure parser and decoder
        sinkpad.add_probe(gst::PadProbeType::BUFFER, move |pad, ref probe_info| {
            match probe_info.data {
                Some(gst::PadProbeData::Buffer(ref buffer)) => {
                    // Drop buffers until keyframe
                    if buffer.flags().contains(gst::BufferFlags::DELTA_UNIT) {
                        println!("Dropping delta buffer {:?}", buffer);
                        return gst::PadProbeReturn::Drop;
                    }

                    let dbin = match dbin_weak.upgrade() {
                        Some(dbin) => dbin,
                        _ => return gst::PadProbeReturn::Remove,
                    };

                    let front = match front_weak.upgrade() {
                        Some(front) => front,
                        _ => return gst::PadProbeReturn::Remove,
                    };

                    let rear = match rear_weak.upgrade() {
                        Some(rear) => rear,
                        _ => return gst::PadProbeReturn::Remove,
                    };

                    // Caps must be available at this point
                    let caps = pad.current_caps().unwrap();
                    let s = caps.structure(0).unwrap();
                    let (parser, decoder) = match s.name() {
                        "video/x-h264" => {
                            let parser = gst::ElementFactory::make("h264parse", None).unwrap();
                            let decoder = gst::ElementFactory::make("d3d11h264dec", None).unwrap();
                            (parser, decoder)
                        }
                        "video/x-h265" => {
                            let parser = gst::ElementFactory::make("h265parse", None).unwrap();
                            let decoder = gst::ElementFactory::make("d3d11h265dec", None).unwrap();
                            (parser, decoder)
                        }
                        _ => unreachable!(),
                    };

                    let convert = gst::ElementFactory::make("d3d11convert", None).unwrap();
                    let capsfilter = gst::ElementFactory::make("capsfilter", None).unwrap();
                    use std::str::FromStr;
                    let caps =
                        gst::Caps::from_str("video/x-raw(memory:D3D11Memory),format=RGBA").unwrap();
                    capsfilter.set_property("caps", caps);

                    // Don't hold too many buffers
                    let queue = gst::ElementFactory::make("queue", None).unwrap();
                    queue.set_property("max-size-buffers", 16u32);
                    queue.set_property("max-size-time", gst::ClockTime::ZERO);
                    queue.set_property("max-size-bytes", 0u32);

                    let clocksync = gst::ElementFactory::make("clocksync", None).unwrap();

                    // Increase ts-offset to compensate latency
                    let srcpad = clocksync.static_pad("src").unwrap();
                    srcpad.add_probe(gst::PadProbeType::BUFFER, |pad, _info| {
                        // Query latency so that clocksync can update latency
                        // XXX: this should be done automatically by parent
                        // pipeline but we disabled SINK flag on proxysink.
                        // Do this manually
                        let clocksync = pad.parent().unwrap();
                        let mut query = gst::query::Latency::new();
                        let ret = clocksync
                            .downcast_ref::<gst::Element>()
                            .unwrap()
                            .query(&mut query);
                        println!("Latency ret {}, {:?}", ret, query);

                        let mut ts_offset = clocksync.property::<i64>("ts-offset");

                        ts_offset += (100 * gst::ClockTime::MSECOND).nseconds() as i64;
                        clocksync.set_property("ts-offset", ts_offset);

                        gst::PadProbeReturn::Remove
                    });

                    dbin.add_many(&[&parser, &decoder, &convert, &capsfilter, &queue, &clocksync])
                        .unwrap();
                    parser.sync_state_with_parent().unwrap();
                    decoder.sync_state_with_parent().unwrap();
                    convert.sync_state_with_parent().unwrap();
                    capsfilter.sync_state_with_parent().unwrap();
                    queue.sync_state_with_parent().unwrap();
                    clocksync.sync_state_with_parent().unwrap();

                    gst::Element::link_many(&[
                        &front,
                        &parser,
                        &decoder,
                        &convert,
                        &capsfilter,
                        &queue,
                        &clocksync,
                        &rear,
                    ])
                    .unwrap();

                    gst::PadProbeReturn::Remove
                }
                _ => {
                    unreachable!();
                }
            }
        });

        dbin.upcast()
    }

    fn add_displaybin(&self) {
        let mut inner = self.inner.lock().unwrap();
        if inner.decodebin.is_none() {
            let dbin = self.create_decodebin();

            inner.pipeline.add(&dbin).unwrap();
            dbin.sync_state_with_parent().unwrap();
            dbin.link(&inner.decode_tee).unwrap();
            inner.tee.link(&dbin).unwrap();

            inner.decodebin = Some(dbin)
        }

        let proxysink = gst::ElementFactory::make("proxysink", None).unwrap();
        // XXX: proxysink doesn't post EOS message
        // Drop sink flag so that pipeline can post EOS message
        // Fixed in https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/3472
        proxysink.unset_element_flags(gst::ElementFlags::SINK);
        inner.pipeline.add(&proxysink).unwrap();
        proxysink.sync_state_with_parent().unwrap();

        let displaybin = Displaybin::new(&proxysink);
        displaybin.run();
        inner.displaybin.push(displaybin);

        inner.decode_tee.link(&proxysink).unwrap();
    }
}

// Keyboard input handler
struct KeyController {
    sourcebin: Mutex<Option<SourceBin>>,
    playbin: Option<gst::Pipeline>,
    playing: bool,
    rate: f64,
}

fn play_manifest(
    main_loop: &glib::MainLoop,
    reader: &ManifestReader,
    controller: &Arc<Mutex<KeyController>>,
) {
    let playbin = gst::ElementFactory::make("playbin", None).unwrap();

    playbin.set_property("uri", "appsrc://");

    let reader_weak = reader.downgrade();
    playbin.connect("source-setup", false, move |values| {
        let src = values[1]
            .get::<gst::Element>()
            .expect("Wrong signal argument type");

        let reader = reader_weak.upgrade().unwrap();

        let appsrc = src.downcast::<gst_app::AppSrc>().unwrap();
        appsrc.set_stream_type(gst_app::AppStreamType::Seekable);
        appsrc.set_format(gst::Format::Time);
        appsrc.set_duration(reader.duration());
        // Make appsrc hold up to 2 buffers, to save memory
        appsrc.set_property("max-buffers", 2u64);
        appsrc.set_property("block", true);
        // We need to update segment manually, specifically for non-zero
        // start timestamp
        appsrc.set_property("handle-segment-change", true);
        let srcpad = appsrc.static_pad("src").unwrap();

        // Add probe to handle seek event
        let reader_weak = reader.downgrade();
        srcpad
            .add_probe(gst::PadProbeType::EVENT_UPSTREAM, move |_, info| {
                if let Some(gst::PadProbeData::Event(ref ev)) = info.data {
                    if let gst::EventView::Seek(seek) = ev.view() {
                        let reader = reader_weak.upgrade().unwrap();
                        reader.prepare_seek(&seek);
                    }
                }

                gst::PadProbeReturn::Ok
            })
            .unwrap();

        let reader_weak = reader.downgrade();
        let reader_other_weak = reader.downgrade();
        appsrc.set_callbacks(
            gst_app::AppSrcCallbacks::builder()
                .need_data(move |appsrc, _size| {
                    let reader = reader_weak.upgrade().unwrap();

                    match reader.state() {
                        ManifestState::Eos | ManifestState::Error => {
                            let _ = appsrc.end_of_stream();
                            return;
                        }
                        ManifestState::Seeking => {
                            return;
                        }
                        _ => (),
                    }

                    let sample = reader.read_data();
                    if sample.is_none() {
                        println!("Empty buffer, EOS");
                        let _ = appsrc.end_of_stream();
                    } else {
                        appsrc.push_sample(sample.as_ref().unwrap()).unwrap();
                    }
                })
                .seek_data(move |appsrc, pos| {
                    let reader = reader_other_weak.upgrade().unwrap();
                    let time_pos = gst::ClockTime::from_nseconds(pos);
                    let seek_rst = reader.seek(time_pos);
                    match seek_rst {
                        gst::FlowReturn::Ok => true,
                        gst::FlowReturn::Eos => {
                            println!("Seek result EOS");
                            let _ = appsrc.end_of_stream();
                            true
                        }
                        _ => {
                            println!("Seek failed");
                            false
                        }
                    }
                })
                .build(),
        );

        None
    });

    let main_loop_clone = main_loop.clone();
    let bus = playbin.bus().unwrap();
    bus.add_watch(move |_, msg| {
        use gst::MessageView;
        let main_loop = &main_loop_clone;

        match msg.view() {
            MessageView::Eos(..) => {
                println!("received eos");
                main_loop.quit()
            }
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.src().map(|s| s.path_string()),
                    err.error(),
                    err.debug()
                );
                main_loop.quit();
            }
            _ => (),
        };

        glib::Continue(true)
    })
    .unwrap();

    playbin.set_state(gst::State::Playing).unwrap();
    let mut controller_lock = controller.lock().unwrap();
    controller_lock.playbin = Some(playbin.downcast_ref::<gst::Pipeline>().unwrap().clone());
    controller_lock.playing = true;
    drop(controller_lock);

    main_loop.run();

    let mut controller_lock = controller.lock().unwrap();
    controller_lock.playbin = None;
    controller_lock.playing = false;
    drop(controller_lock);
    playbin.set_state(gst::State::Null).unwrap();
}

fn do_seek(pipeline: &gst::Pipeline, postion: u64, rate: f64) {
    let pos = gst::ClockTime::from_nseconds(postion);
    println!("Seek to {}, rate {}", pos, rate);

    let event = if rate < 0.0 {
        gst::event::Seek::new(
            rate,
            gst::SeekFlags::FLUSH | gst::SeekFlags::ACCURATE,
            gst::SeekType::Set,
            gst::GenericFormattedValue::Time(Some(gst::ClockTime::ZERO)),
            gst::SeekType::Set,
            gst::GenericFormattedValue::Time(Some(pos)),
        )
    } else {
        gst::event::Seek::new(
            rate,
            gst::SeekFlags::FLUSH | gst::SeekFlags::ACCURATE,
            gst::SeekType::Set,
            gst::GenericFormattedValue::Time(Some(pos)),
            gst::SeekType::Set,
            gst::GenericFormattedValue::Time(None),
        )
    };

    pipeline.send_event(event);
}

fn relative_seek(pipeline: &gst::Pipeline, percent: f64, rate: f64) {
    let dur = pipeline.query_duration::<gst::ClockTime>();
    if dur.is_none() {
        println!("Unknown duration");
        return;
    }
    let duration = dur.unwrap().nseconds();

    let pos = pipeline.query_position::<gst::ClockTime>();
    if pos.is_none() {
        println!("Unknown position");
        return;
    }
    let position = pos.unwrap().nseconds();

    let step = ((duration as f64) * percent) as u64;
    let step = std::cmp::max(step, gst::ClockTime::from_seconds(1).nseconds());

    let pos = if percent < 0.0 {
        if step > position {
            0
        } else {
            position - step
        }
    } else if duration < position + step {
        return;
    } else {
        position + step
    };

    do_seek(pipeline, pos, rate);
}

fn main() -> Result<(), Error> {
    gst::init()?;

    let args = Cli::parse();

    println!(
        "Start transmuxing \"{}\", output file \"{}\"",
        args.uri,
        args.output.display()
    );

    let main_loop = glib::MainLoop::new(None, false);
    let sourcebin = SourceBin::new(&args.uri, &args.output);
    let controller = Arc::new(Mutex::new(KeyController {
        sourcebin: Mutex::new(Some(sourcebin.clone())),
        playbin: None,
        playing: false,
        rate: 1.0,
    }));

    let controller_clone = controller.clone();
    // Handle keyboard input
    // 's' or 'S': Stop recording
    // Left arrow: Backward seeking
    // Right arrow: Forward seeking
    // Up arrow: Increase rate
    // Down arrow: Decrease rate
    std::thread::spawn(move || {
        const SPACE: u16 = 0x20;
        const LEFT: u16 = 0x25;
        const UP: u16 = 0x26;
        const RIGHT: u16 = 0x27;
        const DOWN: u16 = 0x28;

        loop {
            if let KeyEvent(key) = WinConsole::input().read_single_input().unwrap() {
                let mut controller = controller_clone.lock().unwrap();
                if key.key_down {
                    let char_value = key.u_char;
                    if char_value.is_ascii_alphanumeric() || char_value.is_ascii_punctuation() {
                        if char_value == 's' || char_value == 'S' {
                            let sourcebin = controller.sourcebin.lock().unwrap();
                            if let Some(sourcebin) = sourcebin.as_ref() {
                                sourcebin.stop();
                            }
                        } else if char_value == 'a' || char_value == 'A' {
                            let sourcebin = controller.sourcebin.lock().unwrap();
                            if let Some(sourcebin) = sourcebin.as_ref() {
                                sourcebin.add_displaybin();
                            }
                        }
                    } else if controller.playbin.is_some() {
                        let playbin = controller.playbin.as_ref().unwrap().clone();
                        match key.virtual_key_code {
                            SPACE => {
                                if controller.playing {
                                    controller.playing = false;
                                    println!("Pausing");
                                    playbin.set_state(gst::State::Paused).unwrap();
                                } else {
                                    controller.playing = true;
                                    println!("Playing");
                                    playbin.set_state(gst::State::Playing).unwrap();
                                }
                            }
                            LEFT => {
                                relative_seek(&playbin, -0.01, controller.rate);
                            }
                            RIGHT => {
                                relative_seek(&playbin, 0.05, controller.rate);
                            }
                            UP => {
                                controller.rate += 0.5;
                                if controller.rate > 4.0 {
                                    controller.rate = 4.0;
                                } else if controller.rate == 0.0 {
                                    controller.rate = 0.5;
                                }

                                println!("Set rate {}", controller.rate);

                                let pos = playbin.query_position::<gst::ClockTime>();
                                if let Some(pos) = pos {
                                    do_seek(&playbin, pos.nseconds(), controller.rate);
                                }
                            }
                            DOWN => {
                                controller.rate -= 0.5;
                                if controller.rate < -4.0 {
                                    controller.rate = -4.0;
                                } else if controller.rate == 0.0 {
                                    controller.rate = -0.5;
                                }

                                println!("Set rate {}", controller.rate);

                                let pos = playbin.query_position::<gst::ClockTime>();
                                if let Some(pos) = pos {
                                    do_seek(&playbin, pos.nseconds(), controller.rate);
                                }
                            }

                            _ => (),
                        }
                    }
                }
            };
        }
    });

    println!("Running source pipeline");
    let manifest = sourcebin.run();
    println!("End source pipeline");

    let controller_lock = controller.lock().unwrap();
    let mut sourcebin_lock = controller_lock.sourcebin.lock().unwrap();
    *sourcebin_lock = None;
    drop(sourcebin_lock);
    drop(controller_lock);

    println!(
        "Have {} fragments with total duration {}",
        manifest.fragments.len(),
        manifest.total_duration
    );

    if manifest.fragments.is_empty() {
        println!("No recorded fragment");
        return Ok(());
    }

    let reader = ManifestReader::new(&args.output, &manifest).unwrap();

    play_manifest(&main_loop, &reader, &controller);

    Ok(())
}
