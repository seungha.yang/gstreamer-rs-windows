use crate::gst::subclass::prelude::ObjectSubclassIsExt;
use gst::glib;

mod imp;

glib::wrapper! {
    pub struct RtspInput(ObjectSubclass<imp::RtspInput>) @extends gst::Pipeline, gst::Bin, gst::Element, gst::Object;
}

unsafe impl Send for RtspInput {}
unsafe impl Sync for RtspInput {}

impl RtspInput {
    pub fn new(name: Option<&str>, uri: &str) -> Self {
        glib::Object::new(&[("name", &name), ("uri", &uri)]).unwrap()
    }

    pub fn set_appsrc(&self, appsrc: &gst_app::AppSrc) {
        let imp = self.imp();
        imp.set_appsrc(appsrc);
    }
}
