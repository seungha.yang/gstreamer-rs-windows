use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Mutex,
};

use once_cell::sync::Lazy;

#[derive(Default)]
struct Settings {
    uri: Option<String>,
    appsrc: Option<gst_app::AppSrc>,
}

#[derive(Default)]
struct State {
    latency: Option<gst::ClockTime>,
}

pub struct RtspInput {
    settings: Mutex<Settings>,
    state: Mutex<State>,
    got_error: AtomicBool,
    dbin: gst::Element,
    appsink: gst_app::AppSink,
}

#[glib::object_subclass]
impl ObjectSubclass for RtspInput {
    const NAME: &'static str = "RtspInput";
    type Type = super::RtspInput;
    type ParentType = gst::Pipeline;

    fn new() -> Self {
        let dbin = gst::ElementFactory::make("uridecodebin", None).unwrap();
        let appsink = gst::ElementFactory::make("appsink", None)
            .unwrap()
            .downcast::<gst_app::AppSink>()
            .unwrap();

        Self {
            settings: Mutex::new(Settings::default()),
            state: Mutex::new(State::default()),
            got_error: AtomicBool::new(false),
            dbin,
            appsink,
        }
    }
}

impl ObjectImpl for RtspInput {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![glib::ParamSpecString::new(
                "uri",
                "URI",
                "URI to use",
                None,
                glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
            )]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "uri" => {
                let uri: Option<String> = value.get().expect("type checked upstream");
                let mut settings = self.settings.lock().unwrap();
                settings.uri = uri;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "uri" => {
                let settings = self.settings.lock().unwrap();
                settings.uri.to_value()
            }
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        let settings = self.settings.lock().unwrap();
        let uri = settings.uri.as_ref().unwrap();

        use std::str::FromStr;
        let caps = gst::Caps::from_str("video/x-h264; video/x-h265").unwrap();

        self.dbin.set_property("caps", caps);
        self.dbin.set_property("uri", uri);

        self.dbin.connect("source-setup", false, move |args| {
            let source = args[1].get::<gst::Element>().unwrap();
            source.set_property_from_str("protocols", "tcp");
            // 500ms latency
            source.set_property("latency", 500u32);
            None
        });

        let obj_weak = obj.downgrade();
        self.dbin.connect_pad_added(move |_, pad| {
            let obj = match obj_weak.upgrade() {
                Some(obj) => obj,
                None => return,
            };

            let caps = pad.current_caps().unwrap();
            let s = caps.structure(0).unwrap();

            match s.name() {
                "video/x-h264" => (),
                "video/x-h265" => (),
                _ => return,
            };

            println!("Pad added with {}", s.name());

            let imp = obj.imp();
            let sinkpad = imp.appsink.static_pad("sink").unwrap();
            if !sinkpad.is_linked() {
                pad.link(&sinkpad).unwrap();
            }
        });

        obj.add_many(&[&self.dbin, self.appsink.upcast_ref()])
            .unwrap();

        let obj_weak = obj.downgrade();
        let obj_other_weak = obj.downgrade();
        self.appsink.set_callbacks(
            gst_app::AppSinkCallbacks::builder()
                .new_sample(move |appsink| {
                    let obj = match obj_weak.upgrade() {
                        Some(obj) => obj,
                        None => return Err(gst::FlowError::Eos),
                    };

                    let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;
                    let imp = obj.imp();
                    let settings = imp.settings.lock().unwrap();
                    if let Some(peer) = settings.appsrc.as_ref() {
                        let peer = peer.clone();
                        drop(settings);

                        let mut state = imp.state.lock().unwrap();
                        if let Some(latency) = state.latency.take() {
                            drop(state);
                            println!("Set latency {}", latency);

                            peer.set_latency(latency, gst::ClockTime::NONE);
                        }

                        peer.push_sample(&sample)
                    } else {
                        Err(gst::FlowError::Error)
                    }
                })
                .eos(move |_appsink| {
                    let obj = match obj_other_weak.upgrade() {
                        Some(obj) => obj,
                        None => return,
                    };

                    let imp = obj.imp();
                    let settings = imp.settings.lock().unwrap();
                    if let Some(peer) = settings.appsrc.as_ref() {
                        let peer = peer.clone();
                        drop(settings);
                        let _ = peer.end_of_stream();
                    }
                })
                .build(),
        );

        let obj_weak = obj.downgrade();
        let sinkpad = self.appsink.static_pad("sink").unwrap();
        sinkpad.add_probe(gst::PadProbeType::EVENT_UPSTREAM, move |_pad, info| {
            if let Some(gst::PadProbeData::Event(ref ev)) = info.data {
                use gst::EventView;

                if let EventView::Latency(ev) = ev.view() {
                    let obj = match obj_weak.upgrade() {
                        Some(obj) => obj,
                        None => return gst::PadProbeReturn::Remove,
                    };

                    let latency = ev.latency();
                    let imp = obj.imp();
                    let mut state = imp.state.lock().unwrap();

                    state.latency = Some(latency);
                }
            }
            gst::PadProbeReturn::Ok
        });
    }
}

impl GstObjectImpl for RtspInput {}

impl ElementImpl for RtspInput {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "RtspInput",
                "Source",
                "RtspInput element",
                "Seungha Yang <seungha@centricular.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }
}

impl BinImpl for RtspInput {
    fn handle_message(&self, bin: &super::RtspInput, msg: gst::Message) {
        use gst::MessageView;

        if let MessageView::Error(err) = msg.view() {
            if self
                .got_error
                .compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst)
                .is_ok()
            {
                println!("Got error {:?}", err);
                let settings = self.settings.lock().unwrap();
                if let Some(peer) = settings.appsrc.as_ref() {
                    let peer = peer.clone();
                    drop(settings);
                    let _ = peer.end_of_stream();
                }
            }

            return;
        }

        self.parent_handle_message(bin, msg)
    }
}

impl PipelineImpl for RtspInput {}

impl RtspInput {
    pub fn set_appsrc(&self, appsrc: &gst_app::AppSrc) {
        let mut settings = self.settings.lock().unwrap();
        settings.appsrc = Some(appsrc.clone());
    }
}
