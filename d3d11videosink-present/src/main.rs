use gst::glib;
use gst::prelude::*;
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};
use std::time::SystemTime;
use windows::{
    core::*,
    Win32::Graphics::{
        Direct2D::Common::*, Direct2D::*, Direct3D11::*, DirectWrite::*, Dxgi::Common::*, Dxgi::*,
    },
};

struct OverlayContext {
    d2d_factory: ID2D1Factory,
    dwrite_factory: IDWriteFactory,
    text_format: IDWriteTextFormat,
    texture_desc: D3D11_TEXTURE2D_DESC,
    text_layout: Option<IDWriteTextLayout>,
    timestamp_queue: VecDeque<SystemTime>,
    avg_fps: f32,
    display_fps: f32,
    font_size: f32,
}

unsafe impl Send for OverlayContext {}

fn create_overlay_context() -> Arc<Mutex<OverlayContext>> {
    let d2d_factory = unsafe {
        D2D1CreateFactory::<ID2D1Factory>(D2D1_FACTORY_TYPE_MULTI_THREADED, std::ptr::null())
            .unwrap()
    };
    let dwrite_factory = unsafe {
        let unknown =
            DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, &IDWriteFactory::IID).unwrap();
        unknown.cast::<IDWriteFactory>().unwrap()
    };

    // font size can be updated later
    let text_format = unsafe {
        dwrite_factory
            .CreateTextFormat(
                w!("Consolas"),
                None,
                DWRITE_FONT_WEIGHT_REGULAR,
                DWRITE_FONT_STYLE_NORMAL,
                DWRITE_FONT_STRETCH_NORMAL,
                12f32,
                w!("en-us"),
            )
            .unwrap()
    };

    Arc::new(Mutex::new(OverlayContext {
        d2d_factory,
        dwrite_factory,
        text_format,
        texture_desc: Default::default(),
        text_layout: None,
        timestamp_queue: VecDeque::with_capacity(10),
        avg_fps: 0f32,
        display_fps: 0f32,
        font_size: 12f32,
    }))
}

fn main() -> Result<()> {
    gst::init().unwrap();

    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        println!("URI must be specified");
        return Ok(());
    }

    let main_loop = glib::MainLoop::new(None, false);

    let overlay_context = create_overlay_context();
    let overlay_context_weak = Arc::downgrade(&overlay_context);
    let videosink = gst::ElementFactory::make("d3d11videosink", None).unwrap();
    // Needs BGRA swapchain for D2D interop
    videosink.set_property_from_str("display-format", "DXGI_FORMAT_B8G8R8A8_UNORM");

    // Listen "present" signal and draw overlay from the callback
    // Required operations here:
    // 1) Gets IDXGISurface and ID3D11Texture2D interface from
    //    given ID3D11RenderTargetView COM object
    //   - ID3D11Texture2D: To get texture resolution
    //   - IDXGISurface: To create Direct2D render target
    // 2) Creates or reuse IDWriteTextLayout interface
    //   - This object represent text layout we want to draw on render target
    // 3) Draw rectangle (overlay background) and text on render target
    //
    // NOTE: ID2D1Factory, IDWriteFactory, IDWriteTextFormat, and
    // IDWriteTextLayout objects are device-independent. Which can be created
    // earlier instead of creating them in the callback.
    // But ID2D1RenderTarget is device-dependent resource. Client should not
    // hold the render target outside of this callback scope because
    // the resource must be cleared before releasing/resizing swapchain.
    videosink.connect("present", false, move |values| {
        let _sink = values[0]
            .get::<gst::Element>()
            .expect("Wrong signal argument type");
        let _device = values[1]
            .get::<gst::Object>()
            .expect("Wrong signal argument type");
        let rtv_raw = values[2]
            .get::<glib::Pointer>()
            .expect("Wrong signal argument type");

        let overlay_context = overlay_context_weak.upgrade().unwrap();
        let mut context = overlay_context.lock().unwrap();
        let dwrite_factory = context.dwrite_factory.clone();
        let d2d_factory = context.d2d_factory.clone();

        unsafe {
            let unknown: &windows::core::IUnknown = std::mem::transmute(&rtv_raw);
            // Gets IDxgiResource from RTV
            let rtv = unknown.cast::<ID3D11RenderTargetView>().unwrap();
            let resource = {
                let mut resource = None;
                rtv.GetResource(&mut resource);
                resource.unwrap()
            };

            let texture = resource.cast::<ID3D11Texture2D>().unwrap();
            let desc = {
                let mut desc: D3D11_TEXTURE2D_DESC = Default::default();
                texture.GetDesc(&mut desc);
                desc
            };

            // Window size was updated, creates new text layout
            let calculate_font_size = if desc != context.texture_desc {
                context.texture_desc = desc;
                context.text_layout = None;
                true
            } else {
                false
            };

            // New fps, creates new layout
            if context.avg_fps != context.display_fps {
                context.display_fps = context.avg_fps;
                context.text_layout = None;
            }

            if context.text_layout.is_none() {
                let overlay_string = format!("TextOverlay, Fps {:.1}", context.display_fps);
                let overlay_wstring: Vec<u16> = overlay_string.encode_utf16().collect();
                let layout = dwrite_factory
                    .CreateTextLayout(
                        &overlay_wstring,
                        &context.text_format,
                        desc.Width as f32,
                        desc.Height as f32 / 5f32,
                    )
                    .unwrap();

                // Adjust alignment
                layout
                    .SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER)
                    .unwrap();
                layout
                    .SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER)
                    .unwrap();

                // XXX: This is not an efficient approach.
                // the font size can be pre-calculated for pre-defined
                // window size and string length
                let mut range = DWRITE_TEXT_RANGE {
                    startPosition: 0u32,
                    length: overlay_wstring.len() as u32,
                };

                if calculate_font_size {
                    let mut font_size = 12f32;
                    let mut was_decreased = false;

                    loop {
                        let metrics = layout.GetMetrics().unwrap();
                        layout.GetFontSize2(0, &mut font_size, &mut range).unwrap();

                        if metrics.widthIncludingTrailingWhitespace >= desc.Width as f32 {
                            if font_size > 1f32 {
                                font_size -= 0.5f32;
                                was_decreased = true;
                                layout.SetFontSize(font_size, range).unwrap();
                                continue;
                            }

                            break;
                        }

                        if was_decreased {
                            break;
                        }

                        if metrics.widthIncludingTrailingWhitespace < desc.Width as f32 {
                            if metrics.widthIncludingTrailingWhitespace
                                >= desc.Width as f32 * 0.7f32
                            {
                                break;
                            }

                            font_size += 0.5f32;
                            layout.SetFontSize(font_size, range).unwrap();
                        }
                    }

                    context.font_size = font_size;
                } else {
                    layout.SetFontSize(context.font_size, range).unwrap();
                }

                context.text_layout = Some(layout);
            };

            let dxgi_surf = resource.cast::<IDXGISurface>().unwrap();
            let render_target = d2d_factory
                .CreateDxgiSurfaceRenderTarget(
                    &dxgi_surf,
                    &D2D1_RENDER_TARGET_PROPERTIES {
                        r#type: D2D1_RENDER_TARGET_TYPE_DEFAULT,
                        pixelFormat: D2D1_PIXEL_FORMAT {
                            format: DXGI_FORMAT_B8G8R8A8_UNORM,
                            alphaMode: D2D1_ALPHA_MODE_PREMULTIPLIED,
                        },
                        // zero means default DPI
                        dpiX: 0f32,
                        dpiY: 0f32,
                        usage: D2D1_RENDER_TARGET_USAGE_NONE,
                        minLevel: D2D1_FEATURE_LEVEL_DEFAULT,
                    },
                )
                .unwrap();
            let text_brush = render_target
                .CreateSolidColorBrush(
                    &D2D1_COLOR_F {
                        r: 0f32,
                        g: 0f32,
                        b: 0f32,
                        a: 1f32,
                    },
                    std::ptr::null(),
                )
                .unwrap();
            let overlay_brush = render_target
                .CreateSolidColorBrush(
                    &D2D1_COLOR_F {
                        r: 0f32,
                        g: 0.5f32,
                        b: 0.5f32,
                        a: 0.3f32,
                    },
                    std::ptr::null(),
                )
                .unwrap();

            render_target.BeginDraw();
            // Draw overlay background. It will blend overlay background color
            // with video frame
            render_target.FillRectangle(
                &D2D_RECT_F {
                    left: 0f32,
                    top: 0f32,
                    right: desc.Width as f32,
                    bottom: desc.Height as f32 / 5f32,
                },
                &overlay_brush,
            );

            // Then, renders text
            render_target.DrawTextLayout(
                D2D_POINT_2F { x: 0f32, y: 0f32 },
                context.text_layout.as_ref(),
                &text_brush,
                D2D1_DRAW_TEXT_OPTIONS_NONE,
            );

            let mut tag1 = 0u64;
            let mut tag2 = 0u64;
            // EndDraw may not be successful, ignore results for now
            let _ = render_target.EndDraw(&mut tag1, &mut tag2);
        }

        None
    });

    // Add pad probe to calculate framerate
    let sinkpad = videosink.static_pad("sink").unwrap();
    let overlay_context_weak = Arc::downgrade(&overlay_context);
    sinkpad.add_probe(gst::PadProbeType::BUFFER, move |_, probe_info| {
        if let Some(gst::PadProbeData::Buffer(_)) = probe_info.data {
            let overlay_context = overlay_context_weak.upgrade().unwrap();
            let mut context = overlay_context.lock().unwrap();
            context.timestamp_queue.push_back(SystemTime::now());
            // Updates framerate per 10 frames
            if context.timestamp_queue.len() >= 10 {
                let now = context.timestamp_queue.back().unwrap();
                let front = context.timestamp_queue.front().unwrap();
                let duration = now.duration_since(*front).unwrap().as_millis() as f32;
                context.avg_fps = 1000f32 * (context.timestamp_queue.len() - 1) as f32 / duration;
                context.timestamp_queue.clear();
            }
        }
        gst::PadProbeReturn::Ok
    });

    let playbin = gst::ElementFactory::make("playbin", None).unwrap();
    playbin.set_property("uri", &args[1]);
    playbin.set_property("video-sink", &videosink);

    let main_loop_clone = main_loop.clone();
    let bus = playbin.bus().unwrap();
    bus.add_watch(move |_, msg| {
        use gst::MessageView;

        let main_loop = &main_loop_clone;
        match msg.view() {
            MessageView::Eos(..) => {
                println!("received eos");
                main_loop.quit()
            }
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.src().map(|s| s.path_string()),
                    err.error(),
                    err.debug()
                );
                main_loop.quit();
            }
            _ => (),
        };

        glib::Continue(true)
    })
    .unwrap();

    playbin.set_state(gst::State::Playing).unwrap();

    main_loop.run();

    playbin.set_state(gst::State::Null).unwrap();

    Ok(())
}
